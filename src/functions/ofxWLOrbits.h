//
//  ofxWLOrbits.h
//  exampleOrbiter
//
//  Created by lxinspc on 17/03/2022.
//

#pragma once

#include "ofMain.h"

#include "ofxWLOrbitCircular.h"
#include "ofxWLOrbitElliptical.h"

