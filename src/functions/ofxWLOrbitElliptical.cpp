#include "ofxWLOrbitElliptical.h"

using namespace ofxWithLasers::Orbits;

Elliptical::Elliptical(Options o) {


  //Setup coeeficients to auto update
  _coeffcients = make_shared<Coeffcients>(true, o.base, o.res, o.frameRate);

  //Add a coeffcient for theta (rotational angle in polar coordiantes)
  _coeffcients->set(Alpha::THETA, make_shared<Math::Coeffcient>(o.theta, o.dTheta, Limits::TWO_PI_BOUNDS, Limits::OOB_WRAP));

  //Intertesting variotion could be to add radii as a coeffcient as well, and then allow for animation of that as well
  _r1 = o.r1;
  _r2 = o.r2;

//  setCentre(o.centre);
  _setupRotation(o.rotate);

}



glm::vec3 Elliptical::_calculate() {

  //nice and easy this one - we just calculate unit based position based on THETA
//  s.set(cos(_coeffcients->get(THETA)->value()) * _r1, sin(_coeffcients->get(THETA)->value()) * _r2);

  return glm::vec3(0.0f);

  
}






