/*

  ofxWLOrbit - add on for defining orbiting objects which can be used in other functions

  WithLasers::CircularOrbit - Library Implementation of a circular orbit


*/

#pragma once

//Open Frameworks Includes
#include "ofMain.h"


//Add on Includes
#include "ofxWLCoeffcients.h"
#include "ofxWLDeltaT.h"

//Local Includes
#include "ofxWLOrbit.h"

using namespace ofxWithLasers::Math;

namespace ofxWithLasers {

  namespace Orbits {

    class Elliptical : public Orbit {

      public:

        struct Options {
          Coeffcients::TimeBase base = Coeffcients::FRAME_DELTA_T;
          Timing::Resolution res = Timing::MILLI;
          float frameRate = 60.0f;
          float theta = 0.0f;
          float dTheta;
          glm::vec3 centre = glm::vec3(0.0f);
          glm::vec3 rotate = glm::vec3(0.0f);
          float r1;
          float r2;
        };

        Elliptical(Options o);

      protected:

        glm::vec3 _calculate() override; 

      private:

        float _r1;
        float _r2;

    };                                                  //Elliptical
  }                                                   //Orbits
}                                                   //WithLasers


